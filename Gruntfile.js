module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    watch: {
      css: {
        files: ['css/<%= pkg.name %>.scss'],
        tasks: ['sass', 'concat', 'cssmin']
      },
      scripts: {
        files: ['js/<%= pkg.name %>.js'],
        tasks: ['jshint', 'concat', 'uglify']
      }
      
    },
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },
      build: {
        src: 'js/<%= pkg.name %>.bundle.js',
        dest: 'js/<%= pkg.name %>.bundle.min.js'
      }
    },
    sass: {                              // Task
      dist: {                            // Target
        files: {                         // Dictionary of files
          'css/<%= pkg.name %>.css': 'css/<%= pkg.name %>.scss'       // 'destination': 'source'
        }
      }
    },
    cssmin: {
      compress: {
        files: {
          'css/<%= pkg.name %>.min.css': ['css/<%= pkg.name %>.css']
        }
      }
    },
    jshint: {
      options: {
        curly: true,
        eqeqeq: true,
        eqnull: true,
        browser: true,
        camelcase: true,
        unused: true,
        undef: true,
        noempty: true,
        "globals": {
          "jQuery": true,
          "$": true,
          "moment": true,
          "console": true
        }
      },
      all: ['js/<%= pkg.name %>.js']
    },
    concat: {
      css: {
        src: ['css/normalize.css', 'css/<%= pkg.name %>.css'],
        dest: 'css/<%= pkg.name %>.css'
      },
      scripts: {
        src: ['js/moment.min.js', 'js/sugar-1.3.9-custom.min.js', 'js/<%= pkg.name %>.js'],
        dest: 'js/<%= pkg.name %>.bundle.js'
      }
    }

  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-concat');

  // Default task(s).
  grunt.registerTask('default', ['uglify']);

};