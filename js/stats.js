var Endava = Endava || {};

Endava.Stats = function(el, data, delay, interval){
	this.el = el;
	this.data = data;
	this.current = 1;
	this.max = data.length;
	this.startTime = 0;
	this.delay = delay;
	this.interval = interval;
	this.liCache = [];

};
 
Endava.Stats.prototype = function(){
	
	var setup = function(){
		checkClock.call(this);
		drawClock.call(this);
		drawList.call(this);
		tick.call(this);
		rotate.call(this);
		bindings.call(this);
		analytics.call(this);
	},
 
	analytics = function(){

		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-5329140-1']);
		_gaq.push(['_trackPageview']);

		(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' === document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();

	},

	bindings = function(){

		var toggle = document.getElementById("fullscreen");

		toggle.onclick = function(){

			var el = document.documentElement, 
				rfs = el.requestFullScreen || el.webkitRequestFullScreen || el.mozRequestFullScreen;

			rfs.call(el);
		};

	},

	drawList = function(){

		var ul = document.getElementById(this.el);

		for(var i = 0; i < this.data.length; i++){
			var li = document.createElement("li");
			li.innerHTML = "<img src=\"" + this.data[i].icon + "\" />" + this.data[i].title + "<strong>0</strong><em>0</em></li>";
			li.id = "node-" + this.data[i].id;
			ul.appendChild(li);

			this.liCache.push(li);
		}

	},

	checkClock = function(){

		var t = this;
		var time = moment();
		var hour = parseInt(time.format("H"), 10);

		if(hour < 9){
			this.startTime = time.subtract("days", 1).hour(9).minutes(0).seconds(0);
		}
		else{
			this.startTime = time.hour(9).minutes(0).seconds(0);
		}

		drawClock.call(t);

		setTimeout(function(){
			checkClock.call(t);
		}, 1000);
 
	},

	drawClock = function(){

		var info = document.getElementById("info"); 
		info.innerHTML = "Since <strong>" + this.startTime.format("Ha") + "</strong> on <strong>" + this.startTime.format("dddd") + "</strong>";

	},

	refreshList = function(difference, differenceHuman){
		
		var seconds = difference / 1000;

		for(var i = 0; i < this.data.length; i++){

			this.liCache[i].getElementsByTagName("strong")[0].innerHTML = (seconds * this.data[i].multiplier).format(0);
			this.liCache[i].getElementsByTagName("em")[0].innerHTML = "in the last " + differenceHuman;

		}

	},

	tick = function(){
		
		var t = this;
		var time = moment();
		var difference = time.diff(t.startTime);
		var differenceHuman = time.from(t.startTime);

		refreshList.call(t, difference, differenceHuman);

		setTimeout(function(){
			tick.call(t);
		}, t.delay);

	},


	rotate = function(){

		var t = this,
			next = 0,
			previous = 0;

		if(t.current === t.max){
			next = 0;
		}
		else {
			next = t.current;
		}

		if(t.current === 1){
			previous = t.max - 1;
		}
		else {
			previous = t.current - 2;
		} 

		//target.siblings(".previous").addClass("exit");
		//target.siblings().removeClass("show previous next");

		for(var i = 0; i < t.liCache.length; i++){
			if(t.liCache[i].className === "previous"){
				t.liCache[i].className = "exit";
			}
			else {
				t.liCache[i].className = "";
			}
		}

		t.liCache[previous].className = "previous";
		t.liCache[next].className = "next";
		t.liCache[(t.current - 1)].className = "show";

		t.current += 1;

		if(t.current > t.max){
			t.current = 1;
		}

		setTimeout(function(){
			
			for(var i = 0; i < t.liCache.length; i++){
				if(t.liCache[i].className === "exit"){
					t.liCache[i].className = "";
				}
			}

		}, 500);

		

		setTimeout(function(){
			rotate.call(t);
		}, t.interval);

	};

	return {
		setup: setup
	};

}();


(function(){

	
	var data = [
		{
			id: 1,
			title: "Searches on google",
			multiplier: 11574,
			icon: "images/icon-search.png"
		},
		{
			id: 2,
			title: "iPhone applications downloaded",
			multiplier: 216,
			icon: "images/icon-mobile.png"
		},
		{
			id: 3,
			title: "Photos uploaded to flickr",
			multiplier: 110,
			icon: "images/icon-camera.png"
		},
		{
			id: 4,
			title: "facebook status updates",
			multiplier: 11583,
			icon: "images/icon-comment.png"
		},
		{
			id: 5,
			title: "tweets sent",
			multiplier: 1633,
			icon: "images/icon-twitter.png"
		},
		{
			id: 6,
			title: "emails sent",
			multiplier: 2800000,
			icon: "images/icon-email.png"
		},
		{
			id: 7,
			title: "minutes spent on skype",
			multiplier: 6166,
			icon: "images/icon-skype.png"
		},
		{
			id: 8,
			title: "firefox downloads",
			multiplier: 28,
			icon: "images/icon-download.png"
		},
		{
			id: 9,
			title: "Visa transaction revenue (&euro;)",
			multiplier: 57077,
			icon: "images/icon-commerce.png"
		},
		{
			id: 10,
			title: "Manchester United page views",
			multiplier: 17,
			icon: "images/icon-network.png"
		},
		{
			id: 11,
			title: "WorldPay transactions",
			multiplier: 254,
			icon: "images/icon-commerce.png"
		}		

	];


	var stats = new Endava.Stats("stats", data, 100, 4000);
	stats.setup();

})();