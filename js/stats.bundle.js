// moment.js
// version : 2.0.0
// author : Tim Wood
// license : MIT
// momentjs.com
(function(t){function n(t,n){return function(e){return u(t.call(this,e),n)}}function e(t){return function(n){return this.lang().ordinal(t.call(this,n))}}function s(){}function r(t){i(this,t)}function a(t){var n=this._data={},e=t.years||t.year||t.y||0,s=t.months||t.month||t.M||0,r=t.weeks||t.week||t.w||0,a=t.days||t.day||t.d||0,i=t.hours||t.hour||t.h||0,u=t.minutes||t.minute||t.m||0,d=t.seconds||t.second||t.s||0,c=t.milliseconds||t.millisecond||t.ms||0;this._milliseconds=c+1e3*d+6e4*u+36e5*i,this._days=a+7*r,this._months=s+12*e,n.milliseconds=c%1e3,d+=o(c/1e3),n.seconds=d%60,u+=o(d/60),n.minutes=u%60,i+=o(u/60),n.hours=i%24,a+=o(i/24),a+=7*r,n.days=a%30,s+=o(a/30),n.months=s%12,e+=o(s/12),n.years=e}function i(t,n){for(var e in n)n.hasOwnProperty(e)&&(t[e]=n[e]);return t}function o(t){return 0>t?Math.ceil(t):Math.floor(t)}function u(t,n){for(var e=t+"";n>e.length;)e="0"+e;return e}function d(t,n,e){var s,r=n._milliseconds,a=n._days,i=n._months;r&&t._d.setTime(+t+r*e),a&&t.date(t.date()+a*e),i&&(s=t.date(),t.date(1).month(t.month()+i*e).date(Math.min(s,t.daysInMonth())))}function c(t){return"[object Array]"===Object.prototype.toString.call(t)}function h(t,n){var e,s=Math.min(t.length,n.length),r=Math.abs(t.length-n.length),a=0;for(e=0;s>e;e++)~~t[e]!==~~n[e]&&a++;return a+r}function f(t,n){return n.abbr=t,x[t]||(x[t]=new s),x[t].set(n),x[t]}function l(t){return t?(!x[t]&&W&&require("./lang/"+t),x[t]):O.fn._lang}function _(t){return t.match(/\[.*\]/)?t.replace(/^\[|\]$/g,""):t.replace(/\\/g,"")}function m(t){var n,e,s=t.match(A);for(n=0,e=s.length;e>n;n++)s[n]=rn[s[n]]?rn[s[n]]:_(s[n]);return function(r){var a="";for(n=0;e>n;n++)a+="function"==typeof s[n].call?s[n].call(r,t):s[n];return a}}function y(t,n){function e(n){return t.lang().longDateFormat(n)||n}for(var s=5;s--&&P.test(n);)n=n.replace(P,e);return nn[n]||(nn[n]=m(n)),nn[n](t)}function M(t){switch(t){case"DDDD":return E;case"YYYY":return N;case"YYYYY":return $;case"S":case"SS":case"SSS":case"DDD":return V;case"MMM":case"MMMM":case"dd":case"ddd":case"dddd":case"a":case"A":return I;case"X":return R;case"Z":case"ZZ":return X;case"T":return j;case"MM":case"DD":case"YY":case"HH":case"hh":case"mm":case"ss":case"M":case"D":case"d":case"H":case"h":case"m":case"s":return J;default:return RegExp(t.replace("\\",""))}}function D(t,n,e){var s,r=e._a;switch(t){case"M":case"MM":r[1]=null==n?0:~~n-1;break;case"MMM":case"MMMM":s=l(e._l).monthsParse(n),null!=s?r[1]=s:e._isValid=!1;break;case"D":case"DD":case"DDD":case"DDDD":null!=n&&(r[2]=~~n);break;case"YY":r[0]=~~n+(~~n>68?1900:2e3);break;case"YYYY":case"YYYYY":r[0]=~~n;break;case"a":case"A":e._isPm="pm"===(n+"").toLowerCase();break;case"H":case"HH":case"h":case"hh":r[3]=~~n;break;case"m":case"mm":r[4]=~~n;break;case"s":case"ss":r[5]=~~n;break;case"S":case"SS":case"SSS":r[6]=~~(1e3*("0."+n));break;case"X":e._d=new Date(1e3*parseFloat(n));break;case"Z":case"ZZ":e._useUTC=!0,s=(n+"").match(K),s&&s[1]&&(e._tzh=~~s[1]),s&&s[2]&&(e._tzm=~~s[2]),s&&"+"===s[0]&&(e._tzh=-e._tzh,e._tzm=-e._tzm)}null==n&&(e._isValid=!1)}function Y(t){var n,e,s=[];if(!t._d){for(n=0;7>n;n++)t._a[n]=s[n]=null==t._a[n]?2===n?1:0:t._a[n];s[3]+=t._tzh||0,s[4]+=t._tzm||0,e=new Date(0),t._useUTC?(e.setUTCFullYear(s[0],s[1],s[2]),e.setUTCHours(s[3],s[4],s[5],s[6])):(e.setFullYear(s[0],s[1],s[2]),e.setHours(s[3],s[4],s[5],s[6])),t._d=e}}function p(t){var n,e,s=t._f.match(A),r=t._i;for(t._a=[],n=0;s.length>n;n++)e=(M(s[n]).exec(r)||[])[0],e&&(r=r.slice(r.indexOf(e)+e.length)),rn[s[n]]&&D(s[n],e,t);t._isPm&&12>t._a[3]&&(t._a[3]+=12),t._isPm===!1&&12===t._a[3]&&(t._a[3]=0),Y(t)}function g(t){var n,e,s,a,o,u=99;for(a=t._f.length;a>0;a--){if(n=i({},t),n._f=t._f[a-1],p(n),e=new r(n),e.isValid()){s=e;break}o=h(n._a,e.toArray()),u>o&&(u=o,s=e)}i(t,s)}function w(t){var n,e=t._i;if(q.exec(e)){for(t._f="YYYY-MM-DDT",n=0;4>n;n++)if(G[n][1].exec(e)){t._f+=G[n][0];break}X.exec(e)&&(t._f+=" Z"),p(t)}else t._d=new Date(e)}function T(n){var e=n._i,s=Z.exec(e);e===t?n._d=new Date:s?n._d=new Date(+s[1]):"string"==typeof e?w(n):c(e)?(n._a=e.slice(0),Y(n)):n._d=e instanceof Date?new Date(+e):new Date(e)}function v(t,n,e,s,r){return r.relativeTime(n||1,!!e,t,s)}function k(t,n,e){var s=U(Math.abs(t)/1e3),r=U(s/60),a=U(r/60),i=U(a/24),o=U(i/365),u=45>s&&["s",s]||1===r&&["m"]||45>r&&["mm",r]||1===a&&["h"]||22>a&&["hh",a]||1===i&&["d"]||25>=i&&["dd",i]||45>=i&&["M"]||345>i&&["MM",U(i/30)]||1===o&&["y"]||["yy",o];return u[2]=n,u[3]=t>0,u[4]=e,v.apply({},u)}function S(t,n,e){var s=e-n,r=e-t.day();return r>s&&(r-=7),s-7>r&&(r+=7),Math.ceil(O(t).add("d",r).dayOfYear()/7)}function b(t){var n=t._i,e=t._f;return null===n||""===n?null:("string"==typeof n&&(t._i=n=l().preparse(n)),O.isMoment(n)?(t=i({},n),t._d=new Date(+n._d)):e?c(e)?g(t):p(t):T(t),new r(t))}function F(t,n){O.fn[t]=O.fn[t+"s"]=function(t){var e=this._isUTC?"UTC":"";return null!=t?(this._d["set"+e+n](t),this):this._d["get"+e+n]()}}function L(t){O.duration.fn[t]=function(){return this._data[t]}}function H(t,n){O.duration.fn["as"+t]=function(){return+this/n}}for(var O,z,C="2.0.0",U=Math.round,x={},W="undefined"!=typeof module&&module.exports,Z=/^\/?Date\((\-?\d+)/i,A=/(\[[^\[]*\])|(\\)?(Mo|MM?M?M?|Do|DDDo|DD?D?D?|ddd?d?|do?|w[o|w]?|W[o|W]?|YYYYY|YYYY|YY|a|A|hh?|HH?|mm?|ss?|SS?S?|X|zz?|ZZ?|.)/g,P=/(\[[^\[]*\])|(\\)?(LT|LL?L?L?|l{1,4})/g,J=/\d\d?/,V=/\d{1,3}/,E=/\d{3}/,N=/\d{1,4}/,$=/[+\-]?\d{1,6}/,I=/[0-9]*[a-z\u00A0-\u05FF\u0700-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+|[\u0600-\u06FF]+\s*?[\u0600-\u06FF]+/i,X=/Z|[\+\-]\d\d:?\d\d/i,j=/T/i,R=/[\+\-]?\d+(\.\d{1,3})?/,q=/^\s*\d{4}-\d\d-\d\d((T| )(\d\d(:\d\d(:\d\d(\.\d\d?\d?)?)?)?)?([\+\-]\d\d:?\d\d)?)?/,B="YYYY-MM-DDTHH:mm:ssZ",G=[["HH:mm:ss.S",/(T| )\d\d:\d\d:\d\d\.\d{1,3}/],["HH:mm:ss",/(T| )\d\d:\d\d:\d\d/],["HH:mm",/(T| )\d\d:\d\d/],["HH",/(T| )\d\d/]],K=/([\+\-]|\d\d)/gi,Q="Month|Date|Hours|Minutes|Seconds|Milliseconds".split("|"),tn={Milliseconds:1,Seconds:1e3,Minutes:6e4,Hours:36e5,Days:864e5,Months:2592e6,Years:31536e6},nn={},en="DDD w W M D d".split(" "),sn="M D H h m s w W".split(" "),rn={M:function(){return this.month()+1},MMM:function(t){return this.lang().monthsShort(this,t)},MMMM:function(t){return this.lang().months(this,t)},D:function(){return this.date()},DDD:function(){return this.dayOfYear()},d:function(){return this.day()},dd:function(t){return this.lang().weekdaysMin(this,t)},ddd:function(t){return this.lang().weekdaysShort(this,t)},dddd:function(t){return this.lang().weekdays(this,t)},w:function(){return this.week()},W:function(){return this.isoWeek()},YY:function(){return u(this.year()%100,2)},YYYY:function(){return u(this.year(),4)},YYYYY:function(){return u(this.year(),5)},a:function(){return this.lang().meridiem(this.hours(),this.minutes(),!0)},A:function(){return this.lang().meridiem(this.hours(),this.minutes(),!1)},H:function(){return this.hours()},h:function(){return this.hours()%12||12},m:function(){return this.minutes()},s:function(){return this.seconds()},S:function(){return~~(this.milliseconds()/100)},SS:function(){return u(~~(this.milliseconds()/10),2)},SSS:function(){return u(this.milliseconds(),3)},Z:function(){var t=-this.zone(),n="+";return 0>t&&(t=-t,n="-"),n+u(~~(t/60),2)+":"+u(~~t%60,2)},ZZ:function(){var t=-this.zone(),n="+";return 0>t&&(t=-t,n="-"),n+u(~~(10*t/6),4)},X:function(){return this.unix()}};en.length;)z=en.pop(),rn[z+"o"]=e(rn[z]);for(;sn.length;)z=sn.pop(),rn[z+z]=n(rn[z],2);for(rn.DDDD=n(rn.DDD,3),s.prototype={set:function(t){var n,e;for(e in t)n=t[e],"function"==typeof n?this[e]=n:this["_"+e]=n},_months:"January_February_March_April_May_June_July_August_September_October_November_December".split("_"),months:function(t){return this._months[t.month()]},_monthsShort:"Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec".split("_"),monthsShort:function(t){return this._monthsShort[t.month()]},monthsParse:function(t){var n,e,s;for(this._monthsParse||(this._monthsParse=[]),n=0;12>n;n++)if(this._monthsParse[n]||(e=O([2e3,n]),s="^"+this.months(e,"")+"|^"+this.monthsShort(e,""),this._monthsParse[n]=RegExp(s.replace(".",""),"i")),this._monthsParse[n].test(t))return n},_weekdays:"Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"),weekdays:function(t){return this._weekdays[t.day()]},_weekdaysShort:"Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_"),weekdaysShort:function(t){return this._weekdaysShort[t.day()]},_weekdaysMin:"Su_Mo_Tu_We_Th_Fr_Sa".split("_"),weekdaysMin:function(t){return this._weekdaysMin[t.day()]},_longDateFormat:{LT:"h:mm A",L:"MM/DD/YYYY",LL:"MMMM D YYYY",LLL:"MMMM D YYYY LT",LLLL:"dddd, MMMM D YYYY LT"},longDateFormat:function(t){var n=this._longDateFormat[t];return!n&&this._longDateFormat[t.toUpperCase()]&&(n=this._longDateFormat[t.toUpperCase()].replace(/MMMM|MM|DD|dddd/g,function(t){return t.slice(1)}),this._longDateFormat[t]=n),n},meridiem:function(t,n,e){return t>11?e?"pm":"PM":e?"am":"AM"},_calendar:{sameDay:"[Today at] LT",nextDay:"[Tomorrow at] LT",nextWeek:"dddd [at] LT",lastDay:"[Yesterday at] LT",lastWeek:"[Last] dddd [at] LT",sameElse:"L"},calendar:function(t,n){var e=this._calendar[t];return"function"==typeof e?e.apply(n):e},_relativeTime:{future:"in %s",past:"%s ago",s:"a few seconds",m:"a minute",mm:"%d minutes",h:"an hour",hh:"%d hours",d:"a day",dd:"%d days",M:"a month",MM:"%d months",y:"a year",yy:"%d years"},relativeTime:function(t,n,e,s){var r=this._relativeTime[e];return"function"==typeof r?r(t,n,e,s):r.replace(/%d/i,t)},pastFuture:function(t,n){var e=this._relativeTime[t>0?"future":"past"];return"function"==typeof e?e(n):e.replace(/%s/i,n)},ordinal:function(t){return this._ordinal.replace("%d",t)},_ordinal:"%d",preparse:function(t){return t},postformat:function(t){return t},week:function(t){return S(t,this._week.dow,this._week.doy)},_week:{dow:0,doy:6}},O=function(t,n,e){return b({_i:t,_f:n,_l:e,_isUTC:!1})},O.utc=function(t,n,e){return b({_useUTC:!0,_isUTC:!0,_l:e,_i:t,_f:n})},O.unix=function(t){return O(1e3*t)},O.duration=function(t,n){var e,s=O.isDuration(t),r="number"==typeof t,i=s?t._data:r?{}:t;return r&&(n?i[n]=t:i.milliseconds=t),e=new a(i),s&&t.hasOwnProperty("_lang")&&(e._lang=t._lang),e},O.version=C,O.defaultFormat=B,O.lang=function(n,e){return n?(e?f(n,e):x[n]||l(n),O.duration.fn._lang=O.fn._lang=l(n),t):O.fn._lang._abbr},O.langData=function(t){return t&&t._lang&&t._lang._abbr&&(t=t._lang._abbr),l(t)},O.isMoment=function(t){return t instanceof r},O.isDuration=function(t){return t instanceof a},O.fn=r.prototype={clone:function(){return O(this)},valueOf:function(){return+this._d},unix:function(){return Math.floor(+this._d/1e3)},toString:function(){return this.format("ddd MMM DD YYYY HH:mm:ss [GMT]ZZ")},toDate:function(){return this._d},toJSON:function(){return O(this).utc().format("YYYY-MM-DD[T]HH:mm:ss.SSS[Z]")},toArray:function(){var t=this;return[t.year(),t.month(),t.date(),t.hours(),t.minutes(),t.seconds(),t.milliseconds()]},isValid:function(){return null==this._isValid&&(this._isValid=this._a?!h(this._a,(this._isUTC?O.utc(this._a):O(this._a)).toArray()):!isNaN(this._d.getTime())),!!this._isValid},utc:function(){return this._isUTC=!0,this},local:function(){return this._isUTC=!1,this},format:function(t){var n=y(this,t||O.defaultFormat);return this.lang().postformat(n)},add:function(t,n){var e;return e="string"==typeof t?O.duration(+n,t):O.duration(t,n),d(this,e,1),this},subtract:function(t,n){var e;return e="string"==typeof t?O.duration(+n,t):O.duration(t,n),d(this,e,-1),this},diff:function(t,n,e){var s,r,a=this._isUTC?O(t).utc():O(t).local(),i=6e4*(this.zone()-a.zone());return n&&(n=n.replace(/s$/,"")),"year"===n||"month"===n?(s=432e5*(this.daysInMonth()+a.daysInMonth()),r=12*(this.year()-a.year())+(this.month()-a.month()),r+=(this-O(this).startOf("month")-(a-O(a).startOf("month")))/s,"year"===n&&(r/=12)):(s=this-a-i,r="second"===n?s/1e3:"minute"===n?s/6e4:"hour"===n?s/36e5:"day"===n?s/864e5:"week"===n?s/6048e5:s),e?r:o(r)},from:function(t,n){return O.duration(this.diff(t)).lang(this.lang()._abbr).humanize(!n)},fromNow:function(t){return this.from(O(),t)},calendar:function(){var t=this.diff(O().startOf("day"),"days",!0),n=-6>t?"sameElse":-1>t?"lastWeek":0>t?"lastDay":1>t?"sameDay":2>t?"nextDay":7>t?"nextWeek":"sameElse";return this.format(this.lang().calendar(n,this))},isLeapYear:function(){var t=this.year();return 0===t%4&&0!==t%100||0===t%400},isDST:function(){return this.zone()<O([this.year()]).zone()||this.zone()<O([this.year(),5]).zone()},day:function(t){var n=this._isUTC?this._d.getUTCDay():this._d.getDay();return null==t?n:this.add({d:t-n})},startOf:function(t){switch(t=t.replace(/s$/,"")){case"year":this.month(0);case"month":this.date(1);case"week":case"day":this.hours(0);case"hour":this.minutes(0);case"minute":this.seconds(0);case"second":this.milliseconds(0)}return"week"===t&&this.day(0),this},endOf:function(t){return this.startOf(t).add(t.replace(/s?$/,"s"),1).subtract("ms",1)},isAfter:function(n,e){return e=e!==t?e:"millisecond",+this.clone().startOf(e)>+O(n).startOf(e)},isBefore:function(n,e){return e=e!==t?e:"millisecond",+this.clone().startOf(e)<+O(n).startOf(e)},isSame:function(n,e){return e=e!==t?e:"millisecond",+this.clone().startOf(e)===+O(n).startOf(e)},zone:function(){return this._isUTC?0:this._d.getTimezoneOffset()},daysInMonth:function(){return O.utc([this.year(),this.month()+1,0]).date()},dayOfYear:function(t){var n=U((O(this).startOf("day")-O(this).startOf("year"))/864e5)+1;return null==t?n:this.add("d",t-n)},isoWeek:function(t){var n=S(this,1,4);return null==t?n:this.add("d",7*(t-n))},week:function(t){var n=this.lang().week(this);return null==t?n:this.add("d",7*(t-n))},lang:function(n){return n===t?this._lang:(this._lang=l(n),this)}},z=0;Q.length>z;z++)F(Q[z].toLowerCase().replace(/s$/,""),Q[z]);F("year","FullYear"),O.fn.days=O.fn.day,O.fn.weeks=O.fn.week,O.fn.isoWeeks=O.fn.isoWeek,O.duration.fn=a.prototype={weeks:function(){return o(this.days()/7)},valueOf:function(){return this._milliseconds+864e5*this._days+2592e6*this._months},humanize:function(t){var n=+this,e=k(n,!t,this.lang());return t&&(e=this.lang().pastFuture(n,e)),this.lang().postformat(e)},lang:O.fn.lang};for(z in tn)tn.hasOwnProperty(z)&&(H(z,tn[z]),L(z.toLowerCase()));H("Weeks",6048e5),O.lang("en",{ordinal:function(t){var n=t%10,e=1===~~(t%100/10)?"th":1===n?"st":2===n?"nd":3===n?"rd":"th";return t+e}}),W&&(module.exports=O),"undefined"==typeof ender&&(this.moment=O),"function"==typeof define&&define.amd&&define("moment",[],function(){return O})}).call(this);
/*
 *  Sugar Library v1.3.9
 *
 *  Freely distributable and licensed under the MIT-style license.
 *  Copyright (c) 2013 Andrew Plummer
 *  http://sugarjs.com/
 *
 * ---------------------------- */
(function(){var k=true,l=null,n=false;function aa(a){return function(){return a}}var p=Object,q=Array,r=RegExp,s=Date,t=String,u=Number,v=Math,ba=p.prototype.toString,ca=typeof global!=="undefined"?global:this,da={},ea=p.defineProperty&&p.defineProperties,x="Array,Boolean,Date,Function,Number,String,RegExp".split(","),ga=fa(x[0]),ha=fa(x[1]),ia=fa(x[2]),y=fa(x[3]),A=fa(x[4]),B=fa(x[5]),C=fa(x[6]);
function fa(a){var b,c;if(/String|Number|Boolean/.test(a))b=a.toLowerCase();c=a==="Array"&&q.isArray||function(d){if(b&&typeof d===b)return k;return ba.call(d)==="[object "+a+"]"};return da[a]=c}function ja(a){if(!a.SugarMethods){ka(a,"SugarMethods",{});D(a,n,n,{extend:function(b,c,d){D(a,d!==n,c,b)},sugarRestore:function(){return la(a,arguments,function(b,c,d){ka(b,c,d.method)})},sugarRevert:function(){return la(a,arguments,function(b,c,d){if(d.qa)ka(b,c,d.Ba);else delete b[c]})}})}}
function D(a,b,c,d){var e=b?a.prototype:a;ja(a);E(d,function(f,h){var i=e[f],j=F(e,f);if(typeof c==="function")h=ma(e[f],h,c);if(c!==n||!e[f])ka(e,f,h);a.SugarMethods[f]={xa:b,method:h,Ba:i,qa:j}})}function G(a,b,c,d,e){var f={};d=B(d)?d.split(","):d;d.forEach(function(h,i){e(f,h,i)});D(a,b,c,f)}function la(a,b,c){var d=b.length===0,e=H(b),f=n;E(a.SugarMethods,function(h,i){if(d||e.indexOf(h)>-1){f=k;c(i.xa?a.prototype:a,h,i)}});return f}
function ma(a,b,c){return function(){return(a&&(c===k||!c.apply(this,arguments))?a:b).apply(this,arguments)}}function ka(a,b,c){if(ea)p.defineProperty(a,b,{value:c,configurable:k,enumerable:n,writable:k});else a[b]=c}function H(a,b){var c=[],d,e;d=0;for(e=a.length;d<e;d++){c.push(a[d]);b&&b.call(a,a[d],d)}return c}function na(a,b,c){H(q.prototype.concat.apply([],q.prototype.slice.call(a,c||0)),b)}function oa(a){if(!a||!a.call)throw new TypeError("Callback is not callable");}
function I(a){return a!==void 0}function K(a){return a===void 0}function pa(a){return a&&typeof a==="object"}function L(a){return!!a&&ba.call(a)==="[object Object]"&&"hasOwnProperty"in a}function F(a,b){return p.hasOwnProperty.call(a,b)}function E(a,b){for(var c in a)if(F(a,c))if(b.call(a,c,a[c],a)===n)break}function qa(a,b){E(b,function(c){a[c]=b[c]});return a}function ra(a){qa(this,a)}ra.prototype.constructor=p;
function sa(a,b,c,d){var e=[];a=parseInt(a);for(var f=d<0;!f&&a<=b||f&&a>=b;){e.push(a);c&&c.call(this,a);a+=d||1}return e}function N(a,b,c){c=v[c||"round"];var d=v.pow(10,v.abs(b||0));if(b<0)d=1/d;return c(a*d)/d}function O(a,b){return N(a,b,"floor")}function P(a,b,c,d){d=v.abs(a).toString(d||10);d=ta(b-d.replace(/\.\d+/,"").length,"0")+d;if(c||a<0)d=(a<0?"-":"+")+d;return d}
function ua(a){if(a>=11&&a<=13)return"th";else switch(a%10){case 1:return"st";case 2:return"nd";case 3:return"rd";default:return"th"}}function va(){return"\t\n\u000b\u000c\r \u00a0\u1680\u180e\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u202f\u205f\u2028\u2029\u3000\ufeff"}function ta(a,b){return q(v.max(0,I(a)?a:1)+1).join(b||"")}function wa(a,b){var c=a.toString().match(/[^/]*$/)[0];if(b)c=(c+b).split("").sort().join("").replace(/([gimy])\1+/g,"$1");return c}
function R(a){B(a)||(a=t(a));return a.replace(/([\\/'*+?|()\[\]{}.^$])/g,"\\$1")}function xa(a,b){var c=typeof a,d,e,f,h,i,j,g;if(c==="string")return a;f=ba.call(a);d=L(a);e=f==="[object Array]";if(a!=l&&d||e){b||(b=[]);if(b.length>1)for(j=b.length;j--;)if(b[j]===a)return"CYC";b.push(a);d=t(a.constructor);h=e?a:p.keys(a).sort();j=0;for(g=h.length;j<g;j++){i=e?j:h[j];d+=i+xa(a[i],b)}b.pop()}else d=1/a===-Infinity?"-0":t(a&&a.valueOf?a.valueOf():a);return c+f+d}
function ya(a){return/^\[object Date|Array|String|Number|RegExp|Boolean|Arguments\]$/.test(ba.call(a))||L(a)}function za(a,b,c){var d=[],e=a.length,f=b[b.length-1]!==n,h;H(b,function(i){if(ha(i))return n;if(f){i%=e;if(i<0)i=e+i}h=c?a.charAt(i)||"":a[i];d.push(h)});return d.length<2?d[0]:d}function Aa(a,b){G(b,k,n,a,function(c,d){c[d+(d==="equal"?"s":"")]=function(){return p[d].apply(l,[this].concat(H(arguments)))}})}ja(p);E(x,function(a,b){ja(ca[b])});
function Kb(a,b,c,d,e,f){var h=a.toFixed(20),i=h.search(/\./);h=h.search(/[1-9]/);i=i-h;if(i>0)i-=1;e=v.max(v.min((i/3).floor(),e===n?c.length:e),-d);d=c.charAt(e+d-1);if(i<-9){e=-3;b=i.abs()-9;d=c.slice(0,1)}return(a/(f?(2).pow(10*e):(10).pow(e*3))).round(b||0).format()+d.trim()}
D(u,n,n,{random:function(a,b){var c,d;if(arguments.length==1){b=a;a=0}c=v.min(a||0,K(b)?1:b);d=v.max(a||0,K(b)?1:b)+1;return O(v.random()*(d-c)+c)}});
D(u,k,n,{log:function(a){return v.log(this)/(a?v.log(a):1)},abbr:function(a){return Kb(this,a,"kmbt",0,4)},metric:function(a,b){return Kb(this,a,"n\u03bcm kMGTPE",4,K(b)?1:b)},bytes:function(a,b){return Kb(this,a,"kMGTPE",0,K(b)?4:b,k)+"B"},isInteger:function(){return this%1==0},isOdd:function(){return!isNaN(this)&&!this.isMultipleOf(2)},isEven:function(){return this.isMultipleOf(2)},isMultipleOf:function(a){return this%a===0},format:function(a,b,c){var d,e,f,h="";if(K(b))b=",";if(K(c))c=".";d=(A(a)?
N(this,a||0).toFixed(v.max(a,0)):this.toString()).replace(/^-/,"").split(".");e=d[0];f=d[1];for(d=e.length;d>0;d-=3){if(d<e.length)h=b+h;h=e.slice(v.max(0,d-3),d)+h}if(f)h+=c+ta((a||0)-f.length,"0")+f;return(this<0?"-":"")+h},hex:function(a){return this.pad(a||1,n,16)},upto:function(a,b,c){return sa(this,a,b,c||1)},downto:function(a,b,c){return sa(this,a,b,-(c||1))},times:function(a){if(a)for(var b=0;b<this;b++)a.call(this,b);return this.toNumber()},chr:function(){return t.fromCharCode(this)},pad:function(a,
b,c){return P(this,a,b,c)},ordinalize:function(){var a=this.abs();a=parseInt(a.toString().slice(-2));return this+ua(a)},toNumber:function(){return parseFloat(this,10)}});G(u,k,n,"round,floor,ceil",function(a,b){a[b]=function(c){return N(this,c,b)}});G(u,k,n,"abs,pow,sin,asin,cos,acos,tan,atan,exp,pow,sqrt",function(a,b){a[b]=function(c,d){return v[b](this,c,d)}});})();
var Endava = Endava || {};

Endava.Stats = function(el, data, delay, interval){
	this.el = el;
	this.data = data;
	this.current = 1;
	this.max = data.length;
	this.startTime = 0;
	this.delay = delay;
	this.interval = interval;
	this.liCache = [];

};
 
Endava.Stats.prototype = function(){
	
	var setup = function(){
		checkClock.call(this);
		drawClock.call(this);
		drawList.call(this);
		tick.call(this);
		rotate.call(this);
		bindings.call(this);
		analytics.call(this);
	},
 
	analytics = function(){

		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-5329140-1']);
		_gaq.push(['_trackPageview']);

		(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' === document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();

	},

	bindings = function(){

		var toggle = document.getElementById("fullscreen");

		toggle.onclick = function(){

			var el = document.documentElement, 
				rfs = el.requestFullScreen || el.webkitRequestFullScreen || el.mozRequestFullScreen;

			rfs.call(el);
		};

	},

	drawList = function(){

		var ul = document.getElementById(this.el);

		for(var i = 0; i < this.data.length; i++){
			var li = document.createElement("li");
			li.innerHTML = "<img src=\"" + this.data[i].icon + "\" />" + this.data[i].title + "<strong>0</strong><em>0</em></li>";
			li.id = "node-" + this.data[i].id;
			ul.appendChild(li);

			this.liCache.push(li);
		}

	},

	checkClock = function(){

		var t = this;
		var time = moment();
		var hour = parseInt(time.format("H"), 10);

		if(hour < 9){
			this.startTime = time.subtract("days", 1).hour(9).minutes(0).seconds(0);
		}
		else{
			this.startTime = time.hour(9).minutes(0).seconds(0);
		}

		drawClock.call(t);

		setTimeout(function(){
			checkClock.call(t);
		}, 1000);
 
	},

	drawClock = function(){

		var info = document.getElementById("info"); 
		info.innerHTML = "Since <strong>" + this.startTime.format("Ha") + "</strong> on <strong>" + this.startTime.format("dddd") + "</strong>";

	},

	refreshList = function(difference, differenceHuman){
		
		var seconds = difference / 1000;

		for(var i = 0; i < this.data.length; i++){

			this.liCache[i].getElementsByTagName("strong")[0].innerHTML = (seconds * this.data[i].multiplier).format(0);
			this.liCache[i].getElementsByTagName("em")[0].innerHTML = "in the last " + differenceHuman;

		}

	},

	tick = function(){
		
		var t = this;
		var time = moment();
		var difference = time.diff(t.startTime);
		var differenceHuman = time.from(t.startTime);

		refreshList.call(t, difference, differenceHuman);

		setTimeout(function(){
			tick.call(t);
		}, t.delay);

	},


	rotate = function(){

		var t = this,
			next = 0,
			previous = 0;

		if(t.current === t.max){
			next = 0;
		}
		else {
			next = t.current;
		}

		if(t.current === 1){
			previous = t.max - 1;
		}
		else {
			previous = t.current - 2;
		} 

		//target.siblings(".previous").addClass("exit");
		//target.siblings().removeClass("show previous next");

		for(var i = 0; i < t.liCache.length; i++){
			if(t.liCache[i].className === "previous"){
				t.liCache[i].className = "exit";
			}
			else {
				t.liCache[i].className = "";
			}
		}

		t.liCache[previous].className = "previous";
		t.liCache[next].className = "next";
		t.liCache[(t.current - 1)].className = "show";

		t.current += 1;

		if(t.current > t.max){
			t.current = 1;
		}

		setTimeout(function(){
			
			for(var i = 0; i < t.liCache.length; i++){
				if(t.liCache[i].className === "exit"){
					t.liCache[i].className = "";
				}
			}

		}, 500);

		

		setTimeout(function(){
			rotate.call(t);
		}, t.interval);

	};

	return {
		setup: setup
	};

}();


(function(){

	
	var data = [
		{
			id: 1,
			title: "Searches on google",
			multiplier: 11574,
			icon: "images/icon-search.png"
		},
		{
			id: 2,
			title: "iPhone applications downloaded",
			multiplier: 216,
			icon: "images/icon-mobile.png"
		},
		{
			id: 3,
			title: "Photos uploaded to flickr",
			multiplier: 110,
			icon: "images/icon-camera.png"
		},
		{
			id: 4,
			title: "facebook status updates",
			multiplier: 11583,
			icon: "images/icon-comment.png"
		},
		{
			id: 5,
			title: "tweets sent",
			multiplier: 1633,
			icon: "images/icon-twitter.png"
		},
		{
			id: 6,
			title: "emails sent",
			multiplier: 2800000,
			icon: "images/icon-email.png"
		},
		{
			id: 7,
			title: "minutes spent on skype",
			multiplier: 6166,
			icon: "images/icon-skype.png"
		},
		{
			id: 8,
			title: "firefox downloads",
			multiplier: 28,
			icon: "images/icon-download.png"
		},
		{
			id: 9,
			title: "Visa transaction revenue (&euro;)",
			multiplier: 57077,
			icon: "images/icon-commerce.png"
		},
		{
			id: 10,
			title: "Manchester United page views",
			multiplier: 17,
			icon: "images/icon-network.png"
		},
		{
			id: 11,
			title: "WorldPay transactions",
			multiplier: 254,
			icon: "images/icon-commerce.png"
		}		

	];


	var stats = new Endava.Stats("stats", data, 100, 4000);
	stats.setup();

})();